CREATE SCHEMA IF NOT EXISTS spotify_top_hits;

CREATE TABLE IF NOT EXISTS spotify_top_hits.SpotifyTopHits (
    playlist_url VARCHAR(255),
    year INT,
    track_id VARCHAR(255),
    track_name VARCHAR(255),
    track_popularity INT,
    album VARCHAR(255),
    artist_id VARCHAR(255),
    artist_name VARCHAR(255),
    artist_genres VARCHAR(255),
    artist_popularity INT,
    danceability FLOAT,
    energy FLOAT,
    key INT,
    loudness FLOAT,
    mode INT,
    speechiness FLOAT,
    acousticness FLOAT,
    instrumentalness FLOAT,
    liveness FLOAT,
    valence FLOAT,
    tempo FLOAT,
    duration_ms INT,
    time_signature INT
);

COPY spotify_top_hits.SpotifyTopHits FROM 'C:\Users\User\Desktop\SpotifyTopHitPlaylist2010to2022.csv' DELIMITER ',' CSV HEADER;

SELECT * FROM spotify_top_hits.SpotifyTopHits